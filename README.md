# 蜗牛库

#### 项目介绍
{**蜗牛库是基于Springboot的开发的源码分享网站，所有源码可在线演示和下载（作者：进<527801280>QQ群找群主） 。项目网址 [https://www.psnail.com](https://www.psnail.com)}

#### 采用技术
前端：freemarker+ajax+jquery+bootstrap
后端：springboot+mybatis
数据库：mysql
服务器：nginx+tomcat


交流群：527801280

#### 网站截图 

1. 网站首页
![输入图片说明](https://images.gitee.com/uploads/images/2018/0809/210820_524d090b_1440138.png "首页.png")
2. 免费源码
![输入图片说明](https://images.gitee.com/uploads/images/2018/0809/210901_a5768c91_1440138.png "免费源码.png")
3. 源码详情
![输入图片说明](https://images.gitee.com/uploads/images/2018/0809/210932_2ba874de_1440138.png "源码详情.png")
4. 在线演示项目一
![输入图片说明](https://images.gitee.com/uploads/images/2018/0809/211012_d5b5479b_1440138.png "在线演示项目二.png")
5. 在线演示项目二
![输入图片说明](https://images.gitee.com/uploads/images/2018/0809/211138_65db5272_1440138.png "在线演示项目一.png")
