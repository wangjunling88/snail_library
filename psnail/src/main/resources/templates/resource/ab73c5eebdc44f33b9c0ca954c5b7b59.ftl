<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="发现源码 - 发现您需要的源码，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="搜索源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>基于struts2+spring+spring jdbc实现的代码分享网</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="${base}/assets/css/to-do.css">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/summernote/summernote.css">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/summernote/summernote-bs3.css">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/style.css" /> 
    <link rel="stylesheet" type="text/css" href="${base}/umeditor/themes/default/css/umeditor.css" />
    
    <link href="${base}/assets/css/bootstrap-grid.min.css" rel="stylesheet">
	 
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  	  <input type="hidden" id="r_id" value="ab73c5eebdc44f33b9c0ca954c5b7b59">

      <#include "../user/public/index/top.ftl">
      <#include "../user/public/index/left.ftl">
      
      <div class="header-fixed skin-blue">
		<aside class="content-wrapper collapse sidebarLeft">
		<div class="content container-fluid sidebarRight animated fadeInUp mail message-list-wrapper"> 
		<div class="panel panel-white">
		<div class="panel-body"> 
		<div class="row">
		    <div class="col-sm-3 col-md-2 mg-btm-30">
		    	<a id="show" class="btn btn-blue btn-sm btn-block">在线演示</a>
		        <a id="down" class="btn btn-green btn-sm btn-block">免费下载</a>
		        **<hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
		        **<a id="wendang" class="btn btn-info btn-sm btn-block">部署资料</a>
		        <div id="downUrl"></div>
		        <#if user?exists>
			        <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
		        	<input type="hidden" id="u_id" value="${user.id}">
			        **<a onclick="$('#answer').hide();$('#bushu').hide();$('#question').show();" class="btn btn-danger btn-sm btn-block">提问问题</a>
			        **<a onclick="$('#answer').hide();$('#bushu').show();$('#question').hide();" class="btn btn-purple btn-sm btn-block">远程帮助</a>
		        </#if>
		    	<#include "../user/public/resources/main/soft.ftl">
		    </div>
		    
		    <div class="col-sm-9 col-md-10">
		    	 <#if user?exists>
	        	 <div class="reply" style="margin:20px 0;display:none;" id="question">
			    	<div class="form-group">
				   	   <div class="alert alert-theme alert-success fade in">
		                    <div class="left">
		                    	<i class="fa fa-check icon"></i>
		                    </div> 
		                    <div class="content">
		                    	您是
			                    <#if (user.vip==1)><strong>VIP用户</strong>！
								<#elseif (user.zanzhu==1)><strong>赞助用户</strong>！ 
								<#else><strong>普通用户</strong>！
								</#if> 
		                    	请在下方编辑器中 <strong>详细描述</strong> 您的问题。
		                    </div>
		                    <div class="right">
		                       	了解：加入VIP群，所有会员全天活跃讨论技术问题。
		                    </div>
		               </div>
		            </div>
			    	<script type="text/plain" id="myQuestion"></script>
			    	<br/>
			    	<button id="sendQuestionButton" class="btn btn-green btn-sm" onclick="sendQuestion()">￥9.9 咨询</button>
	           		<button id="sendQuestionCancel" class="btn btn-default btn-sm" onclick="$('#question').hide();$('#bushu').hide();$('#answer').show();"> 取消</button>
			    </div>
			    <div class="attatchments-wrapper" style="margin:20px 0;display:none;" id="bushu">
		        	<div class="panel panel-white">
						<div class="panel-body"> 
							<div class="reply">
						    	<div class="form-group">
							   	   <div class="alert alert-theme alert-success fade in">
					                    <img src="${base}/assets/icon_png/shangke.png" style="margin-right:5px;"/>
				           				基于struts+spring+spring jdbc实现的代码分享网
					                    <div class="right">
					                       	了解：远程部署服务，仅VIP用户、赞助用户拥有权限。
					                    </div>
					               </div>
					            </div>
						    </div>
				            <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
				            <div class="content">
				                <div class="col-md-4 col-sm-6">
								    <div class="pricingTable">
								        <h3 class="title">初级部署</h3>
								        <div class="price-value">￥29.9
								            <span class="month">30-60分钟</span>
								        </div>
								        <ul class="pricing-content">
								            <li>代码部署</li>
								            <li>30-60分钟问题答疑</li>
								            <li>适合基础不错的初学者</li>
								        </ul>
				                        <input id="qq2" name="qq2" class="form-control" type="text" value="" placeholder="您的QQ号码" style="margin:10px 10%;width:80%;border-radius: 3px;font-size: 12px;">
								        <a id="fuwu2" class="pricingTable-signup red"><span id="span2">申请</span></a>
								    </div>
								</div>
								
								<div class="col-md-4 col-sm-6">
								    <div class="pricingTable">
								        <h3 class="title">中级部署</h3>
								        <div class="price-value">￥59.9
								            <span class="month">1-4小时</span>
								        </div>
								        <ul class="pricing-content">
								            <li>代码部署 + 项目简单讲解</li>
								            <li>1-4小时问题答疑</li>
								            <li>适合基础一般的初学者</li>
								        </ul>
								        <input id="qq3" name="qq3" class="form-control" type="text" value="" placeholder="您的QQ号码" style="margin:10px 10%;width:80%;border-radius: 3px;font-size: 12px;">
								        <a id="fuwu3" class="pricingTable-signup red"><span id="span3">申请</span></a>
								    </div>
								</div>
								
								<div class="col-md-4 col-sm-6">
								    <div class="pricingTable">
								        <h3 class="title">高级部署</h3>
								        <div class="price-value">￥99.9
								            <span class="month">4-8小时</span>
								        </div>
								        <ul class="pricing-content">
								            <li>代码部署 + 项目全面讲解</li>
								            <li>4-8小时问题答疑</li>
								            <li>适合没有基础的小白</li>
								        </ul>
								        <input id="qq4" name="qq4" class="form-control" type="text" value="" placeholder="您的QQ号码" style="margin:10px 10%;width:80%;border-radius: 3px;font-size: 12px;">
								        <a id="fuwu4" class="pricingTable-signup red"><span id="span4">申请</span></a>
								    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
				</#if>
				
		        <div class="attatchments-wrapper" id="answer">
		        	<div class="panel panel-white">
						<div class="panel-body"> 
				            <h3 class="subject">
				        	<img src="${base}/assets/icon_png/sousuo_small.png" style="margin-right:5px;margin-bottom:5px;"/>
				                                基于struts+spring+spring jdbc实现的代码分享网
				                <button type="button" id="like" class="btn btn-default pull-right" data-toggle="dropdown">
				                    <span class="star" id="star">喜欢	</span>
				                </button> 
				            </h3>
				            <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
				            <div class="content">
				                <div class="message">
				                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
				                    <ul>
				                        <li>List Item 1</li>
				                        <li>List Item 2</li>
				                        <li>List Item 3</li>
				                        <li>List Item 4</li>
				                    </ul>
				                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
				                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum ctetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
				                </div>
				            </div>
				        </div>
				    </div>
				    
		        	<div class="panel panel-white border-top-orange">
		                <div class="panel-heading">
		                    <h3 class="panel-title">解答专区</h3>
		                </div>
		                <div class="panel-body">
		                    <div class="panel-group" id="accordion">
		                        
		                        <div class="panel">
		                            <div class="panel-heading">
		                                <h4 class="panel-title">
		                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		                                        <span class="badge bg-light-green squared" style="margin-right:10px;">#1 已解答</span>
		                                         5星评价：非常好的技术解答<span class="pull-right"><img src="${base}/assets/icon_png/star5.png"/></span>
		                                    </a>
		                                </h4>
		                            </div>
		                            <div id="collapseOne" class="panel-collapse collapse in">
		                                <div class="panel-body">
		                                   	 问题：Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		                               		<br/><br/><button id="daanafcfe5b2959a45c2b1a430823e72daef" class="badge bg-light-blue squared">￥1.0 查看此问题答案，答案有10092个字符。</button>
		                                </div>
		                            </div>
		                        </div>
		                        
		                   </div>
		               </div>
		               
		           </div>
		           
		   		</div>  
			</div>
			
		</div> 
		</div> 
		</div>
		</div>
		</aside>
		</div>
      
      <#include "../user/public/index/modal.ftl">
  
  <script src="${base}/umeditor/third-party/jquery.min.js"></script>
  <script src="${base}/umeditor/umeditor.config.js"></script>
  <script src="${base}/umeditor/umeditor.min.js"></script>
  <script src="${base}/umeditor/lang/zh-cn/zh-cn.js"></script>
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="${base}/assets/js/common-scripts.js"></script>
  <script src="${base}/curoAdmin/js/summernote/summernote.min.js"></script>  
  <script src="${base}/curoAdmin/js/message-item-demo.js"></script> 
  <script>
  		var myQuestion = UM.getEditor('myQuestion');
        $(document).ready(function(){
            $("#show").bind("click",function(){
            	var url = window.open();
                var id = $('#r_id').val();
                var allData = {
	　　　　　　　　　　  id:id
	　　　　　　　　  };
		        $.ajax({
	               url:'${base}/resource/show',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:JSON.stringify(allData),
	               success:function(data){
	               		if(data.status==0){
	               			url.close();
		               		alert(data.message);
	               		}else if(data.status==1){
		               		url.location = data.message;
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
	               }
		         });
            });
            $("#down").bind("click",function(){
                var id = $('#r_id').val();
                var allData = {
	　　　　　　　　　　  id:id
	　　　　　　　　  };
		        $.ajax({
	               url:'${base}/resource/down',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:JSON.stringify(allData),
	               success:function(data){
	               		if(data.status==0){
		               		alert(data.message);
	               		}else if(data.status==1){
		               		$("#downUrl").html(data.message);
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
	               }
		         });
            });
            $("#like").bind("click",function(){
                var id = $('#r_id').val();
                var allData = {
	　　　　　　　　　　  id:id
	　　　　　　　　  };
		        $.ajax({
	               url:'${base}/resource/like',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:JSON.stringify(allData),
	               success:function(data){
	               		if(data.status==0){
		               		alert(data.message);
		               		return false;
	               		}else if(data.status==1){
		               		$("#star").html("<img src='${base}/assets/icon_png/xihuan.png' style='width:15px;height:15px;margin-right:5px;margin-bottom:5px;'/>喜欢");
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
	               }
		         });
            });
            $("[id^=fuwu]").bind("click",function(){
            	var id = $('#r_id').val();
		        var way=$(this).attr("id").replace("fuwu","");
		        var qq = $('#qq'+way).val();
            	if(qq==""||typeof(qq)==="undefined"){
					alert("请输入您的QQ号码！以便作者联系您完成项目的部署。");
				    $('#qq'+way).focus();
				    return false;
				}
		        var allData = {
		        	  id:id,
	　　　　　　　　　　      way:way,
					  qq:qq
	　　　　　　　　   };
		        $.ajax({
		           url:'${base}/resource/fuwu',
		           type:'post',
		           contentType:'application/json;charset=UTF-8',
				   dataType:'json',
		           data:JSON.stringify(allData),
		           beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#fuwu"+way).attr({ disabled: "disabled" });
	                $("#span"+way).text("正在将您的远程部署服务申请通过邮件发送给源码作者。。。");
			       },
		           success:function(data){
			           	alert(data.message);
			           		if(data.status==0){
		           			$("#fuwu"+way).removeAttr("disabled");
			                $("#span"+way).text("申请");
		           		}else if(data.status==1){
		                    window.location.reload();
		           		}
		           },
		           error:function(){
						alert("服务器错误！请联系站长");
						$("#fuwu"+way).removeAttr("disabled");
		                $("#span"+way).text("申请");
						
		           }
		         });
	        });
	        $("[id^=daan]").bind("click",function(){
		        var id=$(this).attr("id").replace("daan","");
		        var content=$("#daan"+id).html();
		        var allData = {
	        	   id:id
	　　　　　　　　   };
		        $.ajax({
		           url:'${base}/resource/daan',
		           type:'post',
		           contentType:'application/json;charset=UTF-8',
				   dataType:'json',
		           data:JSON.stringify(allData),
		           beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#daan"+id).attr({ disabled: "disabled" });
	                $("#daan"+id).text("正在将此答案通过邮件发送到您的注册邮箱。。。");
			       },
		           success:function(data){
			           	alert(data.message);
			           	if(data.status==0){
		           			$("#daan"+id).removeAttr("disabled");
			                $("#daan"+id).text(content);
		           		}else if(data.status==1){
		                    window.location.reload();
		           		}
		           },
		           error:function(){
						alert("服务器错误！请联系站长");
						$("#daan"+id).removeAttr("disabled");
		                $("#daan"+id).text(content);
						
		           }
		         });
	        });
		});
		function sendQuestion() {
	        var myarr = [];
	        myarr.push(UM.getEditor('myQuestion').getContent());
	        if(myarr==""){
	          alert("请详细描述问题！");
	          UM.getEditor('myQuestion').focus();
			  return false;
	        }
	        var id = $('#r_id').val();
	        var title = myarr.join("\n");
	        var allData = {
	　　　　　　　　 id:id,
			   title:title
	　　　　　　};
	        $.ajax({
	           url:'${base}/resource/question',
	           type:'post',
	           contentType:'application/json;charset=UTF-8',
			   dataType:'json',
	           data:JSON.stringify(allData),
	           beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#sendQuestionButton").attr({ disabled: "disabled" });
			        $("#sendQuestionCancel").attr({ disabled: "disabled" });
	                $('#sendQuestionButton').text("正在将您的问题通过邮件发送给源码作者。。。");
			    },
	           success:function(data){
	           		alert(data.message);
	           		if(data.status==0){
	           			$('#sendQuestionButton').removeAttr("disabled");
	           			$('#sendQuestionCancel').removeAttr("disabled");
	           			$('#sendQuestionButton').text("￥9.9 咨询");
	           		}else if(data.status==1){
	                    window.location.reload();
	           		}
	           },
	           error:function(){
					alert("服务器错误！请联系站长");
					$('#sendQuestionButton').removeAttr("disabled");
					$('#sendQuestionCancel').removeAttr("disabled");
	           }
	         });
      	}
    </script>
    </body>
</html>