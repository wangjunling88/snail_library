<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>蜗牛库 - 分享您需要的源码项目，在线演示和免费下载。</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${base}/assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="${base}/assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="${base}/assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    <!-- 云标签 -->
    <link href="${base}/assets/css/yunbiaoqian.css" rel="stylesheet">

    <script src="${base}/assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  
    <#include "public/index/top.ftl">
    <#include "public/index/left.ftl">
    <#include "public/index/right.ftl">
    <#include "public/index/bottom.ftl">
    <#include "public/index/modal.ftl">

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="${base}/assets/js/jquery-1.8.3.min.js"></script>
    <script src="${base}/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
    <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="${base}/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="${base}/assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="${base}/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="${base}/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="${base}/assets/js/sparkline-chart.js"></script>    
    <script src="${base}/assets/js/yunbiaoqian.js"></script>    
    
	<script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: '欢迎来到蜗牛库！<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=7401354f503850cf0617cf668e3a2799ee2d74b645098750de556cb0d4f658b1"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="psnail蜗牛库" title="psnail蜗牛库"></a>',
            // (string | mandatory) the text inside the notification
            text: '网站源码支持在线演示和免费下载，仅供学习和研究使用。',
            // (string | optional) the image to display on the left
            image: '${base}/assets/img/logo_48.png',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
        });
        return false;
        });
	</script>
	
  </body>
</html>
