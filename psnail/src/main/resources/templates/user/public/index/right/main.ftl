<#assign base=request.contextPath />
<div class="row mtbox">
	<div class="col-md-2 col-sm-2 col-md-offset-1 box0">
		<div class="box1">
			<img src="${base}/assets/icon_png/yonghu_60.png"/>
			<h3><a href="${base}/user.html"><#include "main/userNumber.ftl"></a></h3>
		</div>
		<p><#include "main/userNumber.ftl">个注册用户</p>
	</div>
	<div class="col-md-2 col-sm-2 box0">
		<div class="box1">
			<img src="${base}/assets/icon_png/zuozhe_60.png"/>
			<h3><a href="${base}/author.html"><#include "main/authorNumber.ftl"></a></h3>
		</div>
		<p><#include "main/authorNumber.ftl">个源码作者</p>
	</div>
	<div class="col-md-2 col-sm-2 box0">
		<div class="box1">
			<img src="${base}/assets/icon_png/daima_60.png"/>
			<h3><a href="${base}/resources.html"><#include "main/resourceNumber.ftl"></a></h3>
		</div>
		<p><#include "main/resourceNumber.ftl">个可演示源码</p>
	</div>
	<div class="col-md-2 col-sm-2 box0">
		<div class="box1">
			<img src="${base}/assets/icon_png/xuqiu_60.png"/>
			<h3><a href="${base}/demand.html"><#include "main/demandNumber.ftl"></a></h3>
		</div>
		<p><#include "main/demandNumber.ftl">个源码需求</p>
	</div>
	
	<div class="col-md-2 col-sm-2 box0">
		<div class="box1">
			<img src="${base}/assets/icon_png/dashang_60.png"/>
			<h3><a href="${base}/zanzhu.html"><#include "main/moneyNumber.ftl"></a></h3>
		</div>
		<p><#include "main/moneyNumber.ftl">元赞助支持</p>
	</div>
</div>