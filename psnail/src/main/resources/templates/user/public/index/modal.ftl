<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loginModal" class="modal fade">
  <div class="form-login"> 
    <h2 class="form-login-heading">登录</h2>
    <div class="login-wrap">
        <input type="text" class="form-control" name="email" id="email" placeholder="邮箱" autofocus>
        <br>
        <input type="password" name="password" id="password" class="form-control" placeholder="密码">
        <label class="checkbox">
            <span class="pull-right">
                <a data-toggle="modal" href="#forgetPassword"> 忘记密码？</a>
            </span>
        </label>
        <button class="btn btn-theme btn-block" id="loginButton" type="submit"><i class="fa fa-lock"></i> 登录</button>
        <hr>
        
        <div class="registration">
           	 没有账号？
            <a href="${base}/zhuce.html"> 注册</a>
        </div>

    </div>
  </div>
</div>
  
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="forgetPassword" class="modal fade">
  <div class="modal-dialog"> 
	  <div class="modal-content">
	      <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">忘记密码 ?</h4>
	      </div>
	      <div class="modal-body">
	          <p>通过电子邮件重置密码.</p>
	          <input type="text" id="email1" name="email" placeholder="邮箱" autocomplete="off" class="form-control placeholder-no-fix">
	      </div>
	      <div class="modal-footer">
	          <button class="btn btn-theme" id="updatePassword" type="button">确定</button>
	      </div>
	  </div>
  </div>
</div>

<#if user?exists>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="personalModal" class="modal fade">
  <div class="form-login"> 
    <h2 class="form-login-heading">修改个人信息</h2>
    <div class="login-wrap">
    <form action="${base}/user/updatePersonal" method="POST" enctype="multipart/form-data">
    	<p>昵称</p>
        <input type="text" class="form-control" name="nicheng" id="nicheng" value="${user.name}" autofocus>
        <br>
    	<p>头像(60*60)</p>
    	<input type="file" name="file"/>
        <br>
        <img src="${base}/user/${user.photo}" class="img-circle" style="width:60px;">
        <br><br>
        <button class="btn btn-theme btn-block" type="submit">更新</button>
    </form>
    </div>
  </div>
</div>
</#if>
<script>
    //$.backstretch("背景图片路径", {speed: 500});
    $(document).ready(function(){
		$("#loginButton").bind("click",function(){
            var email = $('#email').val();
            var password = $('#password').val();
            if(email==""){
			    alert("请输入邮箱！");
			    $('#email').focus();
			    return false;
			}
			if(password==""){
				alert("请输入密码！");
			    $('#password').focus();
			    return false;
			}
			var allData = {
　　　　　　　　　　  email:email,
           		password:password,
　　　　　　　　};
	        $.ajax({
               url:'${base}/user/login',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
               		if(data.status==0){
	               		alert(data.message);
               		}else if(data.status==1){
	                    window.location.reload();
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         });
        });
        $("#updatePassword").bind("click",function(){
            var email1 = $('#email1').val();
	        $.ajax({
               url:'${base}/email/updatePassword',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:email1,
               beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#updatePassword").attr({ disabled: "disabled" });
                    $('#updatePassword').text("邮件正在发送。。。");
			    },
               success:function(data){
               		alert(data.message);
               		if(data.status==0){
               			$('#updatePassword').removeAttr("disabled");
               		}else if(data.status==1){
	                    $('#updatePassword').text("邮件发送成功");
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
					$('#updatePassword').removeAttr("disabled");
               }
	         });
        });
	});
</script>