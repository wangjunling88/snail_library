<section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	注意事项：
                    <div class="content">
                    	1：请先登录，登录后务必认真解决用户提出的问题
                    	<br/>2：待用户评价后，根据用户评分，您将获得对应的酬劳
                    	<br/>3：所得酬劳和评分一一对应：5星100%，4星80%，3星60%，2星40%，1星20%
                    	<br/>4：若您无法解决用户的问题，请联系站长撤销此问题  <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
		   	   		<input type="hidden" id="id" value="${RequestParameters["id"]}"/>
		   	   		咨询源码：<span id="resource"></span>
		   	   		<br/>咨询金额：￥<span id="money"></span>
		   	   		<br/><br/>咨询问题：
                    <div class="content" id="title">
                    	
                    </div>
               </div>
           </div>
           <#if user?exists>
           <#if (user.author==1)>
           <div class="form-group">
               <script id="jieda" type="text/plain"></script>
           </div>
           <button onclick="getContent();" id="sendAnswerButton" class="btn btn-round btn-success"></i>发布答案</button>
   		   </#if>
   		   </#if> 
       </div>
    </div>
  </div>
  </section>
</section>