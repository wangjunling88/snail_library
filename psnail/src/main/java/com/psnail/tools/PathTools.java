package com.psnail.tools;

import java.io.FileNotFoundException;

import org.springframework.util.ResourceUtils;

public class PathTools {
	public static String getPath() {
		String path = "";
		try {
			path = ResourceUtils.getURL("classpath:").getPath().toString();

		} catch (FileNotFoundException e) {
			System.out.println("PathTools/getPath文件未找到。");
		}
		return path;
	}
}
