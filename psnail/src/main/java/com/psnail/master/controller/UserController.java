package com.psnail.master.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.psnail.master.entity.Resource;
import com.psnail.master.service.UserService;
import com.psnail.tools.GsonTools;

@Controller
@RequestMapping("/user")
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	/**
	 * 用户注册
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String zhuce(@RequestBody Map<String, Object> reqMap) {
		String email = reqMap.get("email").toString();
		String name = reqMap.get("name").toString();
		String password = reqMap.get("password").toString();
		String password_confirm = reqMap.get("password_confirm").toString();
		logger.info("UserController/create()接收的参数email：" + email + ",name：" + name + ",password：" + password
				+ ",password_confirm：" + password_confirm);
		Map<String, Object> result = userService.create(email, name, password, password_confirm);
		logger.info("UserController/create()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户登录
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String login(@RequestBody Map<String, Object> reqMap) {
		String email = reqMap.get("email").toString();
		String password = reqMap.get("password").toString();
		logger.info("UserController/login()接收的参数email：" + email + ",password：" + password);
		Map<String, Object> result = userService.login(email, password);
		logger.info("UserController/login()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户设置新密码
	 */
	@RequestMapping(value = "/newpassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String newpassword(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String password = reqMap.get("password").toString();
		String password_confirm = reqMap.get("password_confirm").toString();
		logger.info("UserController/newpassword()接收的参数id：" + id + ",password：" + password + ",password_confirm："
				+ password_confirm);
		Map<String, Object> result = userService.newpassword(id, password, password_confirm);
		logger.info("UserController/newpassword()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 注销账户
	 */
	@RequestMapping(value = "/exit", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String exit() {
		Map<String, Object> result = userService.exit();
		logger.info("UserController/exit()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 清空缓存
	 */
	@RequestMapping(value = "/clearCache", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String clearCache() {
		Map<String, Object> result = userService.clearCache();
		logger.info("UserController/clearCache()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 更新个人信息
	 * 
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/updatePersonal", method = RequestMethod.POST)
	@ResponseBody
	public String updatePersonal(@RequestParam("nicheng") String nicheng, @RequestParam("file") MultipartFile file) {
		logger.info("UserController/updatePersonal()接收的参数nicheng：" + nicheng);
		Map<String, Object> result = userService.updatePersonal(nicheng, file);
		logger.info("UserController/updatePersonal()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户编辑器上传资源图片
	 * 
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/uploadPhoto", method = RequestMethod.POST)
	@ResponseBody
	public String uploadPhoto(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<String, Object>();
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		// 获取文件
		MultipartFile file = multipartRequest.getFile("file");
		if (!file.isEmpty()) {
			result = userService.uploadPhoto(file);
			logger.info("UserController/uploadPhoto()返回结果集：" + result);
		} else {
			result.put("status", 0);
			result.put("message", "文件是空的！");
			logger.info("UserController/uploadPhoto()返回结果集：" + result);
		}
		return GsonTools.toJson(result);
	}

	/**
	 * 发布需求
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sendDemand", method = RequestMethod.POST)
	@ResponseBody
	public String sendDemand(@RequestBody Map<String, Object> reqMap) {
		String title = reqMap.get("title").toString();
		String content = reqMap.get("content").toString();
		logger.info("UserController/sendDemand()接收的参数title：" + title + "，content：" + content);
		Map<String, Object> result = userService.sendDemand(title, content);
		logger.info("UserController/sendDemand()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 发布源码
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sendResource", method = RequestMethod.POST)
	@ResponseBody
	public String sendResource(@RequestBody Map<String, Object> reqMap) {
		String title = reqMap.get("title").toString();
		String content = reqMap.get("content").toString();
		String label = reqMap.get("label").toString();
		String background = reqMap.get("background").toString();
		String address = reqMap.get("address").toString();
		String github = reqMap.get("github").toString();
		String git = reqMap.get("git").toString();
		String baiduyun = reqMap.get("baiduyun").toString();
		Resource r = new Resource();
		r.setTitle(title);
		r.setContent(content);
		r.setLabel(label);
		r.setBackground(background);
		r.setAddress(address);
		r.setGithub(github);
		r.setGit(git);
		r.setBaiduyun(baiduyun);
		r.setOfficial("");
		Map<String, Object> result = userService.sendResource(r);
		logger.info("UserController/sendResource()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户充值通知邮件
	 * 
	 * @return
	 */
	@RequestMapping(value = "/chongzhi", method = RequestMethod.POST)
	@ResponseBody
	public String chongzhi(@RequestBody Map<String, Object> reqMap) {
		String payway = reqMap.get("payway").toString();
		String money = reqMap.get("money").toString();
		logger.info("UserController/chongzhi()接收的参数payway：" + payway + "，money：" + money);
		Map<String, Object> result = userService.chongzhi(payway, money);
		logger.info("UserController/chongzhi()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 根据id获取充值邮件记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getPayrecord", method = RequestMethod.POST)
	@ResponseBody
	public String getPayrecord(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("UserController/getPayrecord()接收的参数id：" + id);
		Map<String, Object> result = userService.getPayrecord(id);
		logger.info("UserController/getPayrecord()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 管理员审查用户充值
	 * 
	 * @return
	 */
	@RequestMapping(value = "/adminAudit", method = RequestMethod.POST)
	@ResponseBody
	public String adminAudit(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String state = reqMap.get("state").toString();
		logger.info("UserController/adminAudit()接收的参数id：" + id + "，state：" + state);
		Map<String, Object> result = userService.adminAudit(id, state);
		logger.info("UserController/adminAudit()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户赞助
	 * 
	 * @return
	 */
	@RequestMapping(value = "/zanzhu", method = RequestMethod.POST)
	@ResponseBody
	public String zanzhu(@RequestBody Map<String, Object> reqMap) {
		String money = reqMap.get("money").toString();
		logger.info("UserController/zanzhu()接收的参数money：" + money);
		Map<String, Object> result = userService.zanzhu(money);
		logger.info("UserController/zanzhu()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户升级赞助权限
	 * 
	 * @return
	 */
	@RequestMapping(value = "/shengji", method = RequestMethod.POST)
	@ResponseBody
	public String shengji() {
		Map<String, Object> result = userService.shengji();
		logger.info("UserController/shengji()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 用户消费记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/mycost", method = RequestMethod.POST)
	@ResponseBody
	public String mycost() {
		Map<String, Object> result = userService.mycost();
		logger.info("UserController/mycost()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 获取vip用户数量
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getNumberVip", method = RequestMethod.POST)
	@ResponseBody
	public String getNumberVip() {
		Map<String, Object> result = userService.getNumberVip();
		logger.info("UserController/getNumberVip()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 开通vip
	 * 
	 * @return
	 */
	@RequestMapping(value = "/vip", method = RequestMethod.POST)
	@ResponseBody
	public String vip() {
		Map<String, Object> result = userService.vip();
		if ("1".equals(result.get("status").toString())) {
			result=userService.updateVip();
		}
		logger.info("UserController/vip()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

}