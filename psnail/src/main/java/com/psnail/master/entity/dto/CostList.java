package com.psnail.master.entity.dto;

import java.util.Date;

public class CostList {
	private String name;
	private String photo;
	private String amount;
	private Date time;

	public String getName() {
		return name;
	}

	public String getPhoto() {
		return photo;
	}

	public String getAmount() {
		return amount;
	}

	public Date getTime() {
		return time;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setTime(Date time) {
		this.time = time;
	}

}
