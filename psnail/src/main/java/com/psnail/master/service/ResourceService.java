package com.psnail.master.service;

import java.math.BigDecimal;
import java.util.Map;

public interface ResourceService {

	Map<String, Object> show(String id);

	Map<String, Object> like(String id);

	Map<String, Object> down(String id);

	Map<String, Object> question(String id, String title);

	Map<String, Object> fuwu(String id, int way, String qq);

	String detail2Html(String id);

	Map<String, Object> daan(String id);

	void daanTicheng(String id);

	Map<String, Object> getQuestionById(String id);

	Map<String, Object> answer(String id, String content);

	Map<String, Object> star(String id, String score);

	void ticheng(String id, BigDecimal bigDecimal);

}
